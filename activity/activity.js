db.hotelRooms.insertOne({
	name: "Single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic nessecities",
	roomsAvailable: 10,
	isAvailable: false,
});
db.hotelRooms.insertMany([
	{
		name: "Double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		roomsAvailable: 5,
		isAvailable: false,
	},
	{
		name: "Queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		roomsAvailable: 15,
		isAvailable: false
	}
]);
db.hotelRooms.find({name: "Double"});
db.hotelRooms.updateOne(
	{name: "Queen"},
        {
            $set: {
				name: "Queen",
				accomodates: 4,
				price: 4000,
				description: "A room with a queen sized bed perfect for a simple getaway",
				roomsAvailable: 0,
				isAvailable: false
            }
        }
);
db.hotelRooms.deleteMany(
	{
		roomsAvailable: 0
	}
);